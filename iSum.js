/*
** getSum function
** Author : Jaewon Kim
** Date   : 2017-02-17
*/
function getSum(n) {
	var iSum = 0;
    for(var i=1; i<=n; i++) {
    	iSum += i;
    }
    
    return iSum;
}